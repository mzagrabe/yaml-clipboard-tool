mod yaml_clipboard_tool;

use crate::yaml_clipboard_tool::config::Config;
use crate::yaml_clipboard_tool::extractor::Extractor;

fn main() {
    let config = Config::new();

    let extractor = Extractor::new(
        &config,
    );

    extractor.extract();
}
