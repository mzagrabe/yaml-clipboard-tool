// {{{ use
use clap::{
    Arg,
    Command,
    crate_authors,
    crate_description,
    crate_name,
    crate_version,
};

use clap_config_file::{
    ArgumentMetaData,
    get_values,
};

use indexmap::{
    indexmap,
};
// }}} use


// {{{ pub struct Config
#[derive(Debug)]
pub struct Config {
    pub display_protocol:                            String,
    pub preprocessor_command:                        String,
    pub primary_selection_yaml_element:              String,
    pub secondary_selection_yaml_element:            String,
    pub clipboard_selection_yaml_element:            String,
    pub postcopy_command:                            String,
    pub postcopy_command_argument_from_yaml_element: String,
    pub print_yaml_file:                             bool,
    pub yaml_file:                                   String,
}
// }}} pub struct Config


// {{{ impl Config
impl Config {
    // {{{ pub fn new
    pub fn new(
    ) -> Self {
        let config_file_defaults = vec![
            "/etc/yaml-clipboard-tool/yaml-clipboard-tool.yaml".to_owned(),
            "~/.yaml-clipboard-tool/yaml-clipboard-tool.yaml".to_owned(),
        ];

        let config_file_name                                 = "config-file";
        let display_protocol_name                            = "display-protocol";
        let preprocessor_command_name                        = "preprocessor-command";
        let primary_selection_yaml_element_name              = "primary-selection-yaml-element";
        let secondary_selection_yaml_element_name            = "secondary-selection-yaml-element";
        let clipboard_selection_yaml_element_name            = "clipboard-selection-yaml-element";
        let postcopy_command_name                            = "postcopy-command";
        let postcopy_command_argument_from_yaml_element_name = "postcopy-command-argument-from-yaml-element";
        let print_yaml_file_name                             = "print-yaml-file";
        let yaml_file_name                                   = "yaml-file";

        let arguments = indexmap!{
            display_protocol_name => ArgumentMetaData {
                short:         'o',
                env:           "YAML_CLIPBOARD_TOOL_DISPLAY_PROTOCOL",
                value_name:    "protocol",
                help:          "The protocol to use to copy data into the clipboard",
                default_value: "wayland",
                required:      false,
                validator:     None,
            },
            preprocessor_command_name => ArgumentMetaData {
                short:         'p',
                env:           "YAML_CLIPBOARD_TOOL_PREPROCESSOR_COMMAND",
                value_name:    "command",
                help:          "An optional command to run on the YAML file before extraction",
                default_value: "",
                required:      false,
                validator:     None,
            },
            primary_selection_yaml_element_name => ArgumentMetaData {
                short:         'P',
                env:           "YAML_CLIPBOARD_TOOL_PRIMARY_SELECTION_YAML_ELEMENT",
                value_name:    "element",
                help:          "The YAML element to copy into the primary clipboard selection",
                default_value: "",
                required:      false,
                validator:     None,
            },
            secondary_selection_yaml_element_name => ArgumentMetaData {
                short:         'S',
                env:           "YAML_CLIPBOARD_TOOL_SECONDARY_SELECTION_YAML_ELEMENT",
                value_name:    "element",
                help:          "The YAML element to copy into the secondary clipboard selection",
                default_value: "",
                required:      false,
                validator:     None,
            },
            clipboard_selection_yaml_element_name => ArgumentMetaData {
                short:         'C',
                env:           "YAML_CLIPBOARD_TOOL_CLIPBOARD_SELECTION_YAML_ELEMENT",
                value_name:    "element",
                help:          "The YAML element to copy into the clipboard clipboard selection",
                default_value: "",
                required:      false,
                validator:     None,
            },
            postcopy_command_name => ArgumentMetaData {
                short:         'a',
                env:           "YAML_CLIPBOARD_TOOL_POSTCOPY_COMMAND",
                value_name:    "command",
                help:          "The command to run after copying yaml elements to the clipboard",
                default_value: "",
                required:      false,
                validator:     None,
            },
            postcopy_command_argument_from_yaml_element_name => ArgumentMetaData {
                short:         'A',
                env:           "YAML_CLIPBOARD_TOOL_POSTCOPY_COMMAND_ARGUMENT_FROM_YAML_ELEMENT",
                value_name:    "element",
                help:          "The YAML element to provide as the argument to the postcopy command",
                default_value: "",
                required:      false,
                validator:     None,
            },
            print_yaml_file_name => ArgumentMetaData {
                short:         'Y',
                env:           "YAML_CLIPBOARD_TOOL_PRINT_YAML_FILE",
                value_name:    "boolean",
                help:          "Boolean value to indicate whether, or not, to print out the YAML file",
                default_value: "true",
                required:      false,
                validator:     None,
            },
        };

        let mut command = Command::new(crate_name!())
            .version(crate_version!())
            .author(crate_authors!("\n"))
            .about(crate_description!())
            .arg(Arg::new(config_file_name)
                .short('c')
                .long(config_file_name)
                .env("YAML_CLIPBOARD_TOOL_CONFIG_FILE")
                .value_name("path")
                .help("Configuration file path")
                .default_values(config_file_defaults)
                .num_args(1)
                .action(clap::ArgAction::Append)
            )
        ;

        for (argument, argument_meta_data) in &arguments {
            command = command.arg(Arg::new(argument)
                .short(argument_meta_data.short)
                .long(argument)
                .env(argument_meta_data.env)
                .value_name(argument_meta_data.value_name)
                .help(argument_meta_data.help)
                .default_value(argument_meta_data.default_value)
                .num_args(1)
            );
        }

        command = command
            .arg(Arg::new(yaml_file_name)
                .help("The YAML file to extract data into the clipboard")
                .required(true)
            )
        ;

        let matches = command.get_matches();

        let values = get_values(
            &arguments,
            &matches,
            config_file_name,
            true,
        );

        let print_yaml_file_value = &values[print_yaml_file_name].value;
        let print_yaml_file: bool = print_yaml_file_value
            .trim()
            .parse()
            .unwrap_or_else(
                |_|
                panic!("Unable to convert '{print_yaml_file_value}' to a bool")
            )
        ;

        let config = Self {
            display_protocol:                            values[display_protocol_name].value.to_owned(),
            preprocessor_command:                        values[preprocessor_command_name].value.to_owned(),
            primary_selection_yaml_element:              values[primary_selection_yaml_element_name].value.to_owned(),
            secondary_selection_yaml_element:            values[secondary_selection_yaml_element_name].value.to_owned(),
            clipboard_selection_yaml_element:            values[clipboard_selection_yaml_element_name].value.to_owned(),
            postcopy_command:                            values[postcopy_command_name].value.to_owned(),
            postcopy_command_argument_from_yaml_element: values[postcopy_command_argument_from_yaml_element_name].value.to_owned(),
            print_yaml_file,
            yaml_file:                                   matches
                .get_one::<String>(yaml_file_name)
                .unwrap()
                .to_string()
            ,
        };

        config
    }
    // }}} pub fn new
}
// }}} impl Config
