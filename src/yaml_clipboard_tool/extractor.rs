// {{{ use
use crate::yaml_clipboard_tool::config::Config;
use yaml_value_extractor::yaml_value_extract;

use std::collections::VecDeque;

use std::{
    io::Write,
    fs,
    process,
    process::{
        Command,
        Stdio,
    },
};

use serde_yaml::{
    from_str,
    Value,
};
// }}} use


// {{{ pub struct Extractor
pub struct Extractor<'a> {
    config: &'a Config,
}
// }}} pub struct Extractor


// {{{ impl Extractor
impl<'a> Extractor<'a> {
    // {{{ pub fn new
    pub fn new(
        config: &'a Config,
    ) -> Extractor<'a> {
        Self {
            config,
        }
    }
    // }}} pub fn new


    // {{{ fn extract
    pub fn extract(
        &self,
    ) {
        let yaml_file_path = self.determine_yaml_file_path();

        let yaml_content = self.extract_yaml_content(
            &yaml_file_path,
        );

        let deserialized_yaml = self.deserialize_yaml_content(
            &yaml_content,
        );

        self.copy_elements_to_clipboard(
            &deserialized_yaml,
        );

        self.postcopy_command(
            &deserialized_yaml,
        );
    }
    // }}} fn extract


    // {{{ fn deserialize_yaml_content
    fn deserialize_yaml_content(
        &self,
        yaml_content: &str,
    ) -> Value {
        match from_str(yaml_content) {
            Ok(deserialized_yaml) => deserialized_yaml,
            Err(err)              => {
                eprintln!("Unable to deserialize YAML from string: {err}");
                process::exit(40);
            }
        }
    }
    // }}} fn deserialize_yaml_content


    // {{{ fn postcopy_command
    fn postcopy_command(
        &self,
        deserialized_yaml: &Value,
    ) {
        if ! self.config.postcopy_command.is_empty() {
            let mut arguments: VecDeque<&str> = self.config.postcopy_command
                .split_whitespace()
                .collect()
            ;

            if let Some(command) = arguments.pop_front() {
                let postcopy_command_argument_from_yaml_element: String;
                if ! self.config.postcopy_command_argument_from_yaml_element.is_empty() {
                    match yaml_value_extract(
                        deserialized_yaml,
                        &self.config.postcopy_command_argument_from_yaml_element,
                    ) {
                        Ok(Some(value)) => {
                            postcopy_command_argument_from_yaml_element = value.to_owned();
                            arguments.push_back(&postcopy_command_argument_from_yaml_element);
                        },
                        Ok(None)        => (),
                        Err(err)        => {
                            eprintln!("Unable to extract YAML value: '{err}'");
                            process::exit(41);
                        },
                    }
                }

                match Command::new(command)
                    .args(&arguments)
                    .stderr(Stdio::null())
                    .spawn()
                {
                    Ok(_)    => (),
                    Err(err) => {
                        eprintln!("Error spawning command: '{err}'");
                        process::exit(42);
                    },
                }
            }
            else {
                eprintln!("Unable to get command from arguments '{arguments:?}'");
                process::exit(43);
            }
        }
    }
    // }}} fn postcopy_command


    // {{{ fn copy_elements_to_clipboard
    fn copy_elements_to_clipboard(
        &self,
        deserialized_yaml: &Value,
    ) {
        self.copy_element_to_clipboard(
            deserialized_yaml,
            "primary",
            &self.config.primary_selection_yaml_element,
        );
        self.copy_element_to_clipboard(
            deserialized_yaml,
            "secondary",
            &self.config.secondary_selection_yaml_element,
        );
        self.copy_element_to_clipboard(
            deserialized_yaml,
            "clipboard",
            &self.config.clipboard_selection_yaml_element,
        );
    }
    // }}} fn copy_elements_to_clipboard


    // {{{ fn copy_element_to_clipboard
    fn copy_element_to_clipboard(
        &self,
        deserialized_yaml:      &Value,
        selection_name:         &str,
        selection_config_value: &str,
    ) {
        if ! selection_config_value.is_empty() {
            let element = match yaml_value_extract(
                deserialized_yaml,
                selection_config_value,
            ) {
                Ok(Some(value)) => value,
                Ok(None)        => return,
                Err(err)        => {
                    eprintln!("Unable to extract YAML value: '{err}'");
                    process::exit(44);
                },
            };

            let clipboard_command: &str;
            let mut arguments:     Vec<&str> = vec![];

            if &self.config.display_protocol == "wayland" {
                clipboard_command = "wl-copy";

                if selection_name == "primary" {
                    arguments = vec![
                        "--primary",
                    ];
                }
                else if selection_name != "clipboard" {
                    eprintln!("No wayland clipboard support for selection '{selection_name}'");
                    process::exit(57);
                }
            }
            else if &self.config.display_protocol == "x" {
                clipboard_command = "xclip";

                arguments = vec![
                    "-selection",
                    selection_name,
                ];
            }
            else {
                eprintln!(
                    "No display protocol support for '{}'",
                    &self.config.display_protocol,
                );
                process::exit(58);
            }

            let mut command = match Command::new(clipboard_command)
                .args(&arguments)
                .stdin(Stdio::piped())
                .spawn()
            {
                Ok(command) => command,
                Err(err)    => {
                    eprintln!("Unable to spawn {clipboard_command} process: '{err}'");
                    process::exit(45);
                },
            };

            let mut command_stdin = match command.stdin.take() {
                Some(command_stdin) => command_stdin,
                None                => {
                    eprintln!("Unable to take stdin of command {clipboard_command}.");
                    process::exit(46);
                },
            };
            match command_stdin
                .write_all(
                    element.as_bytes(),
                )
            {
                Ok(_)    => (),
                Err(err) => {
                    eprintln!("Unable to write bytes to stdin of {clipboard_command} process: '{err}'");
                    process::exit(47);
                },
            };

            // Close stdin to finish and avoid indefinite blocking
            drop(command_stdin);

            match command.wait() {
                Ok(_)    => (),
                Err(err) => {
                    eprintln!("Failed to wait for {clipboard_command}: '{err}'");
                    process::exit(48);
                },
            }
        }
    }
    // }}} fn copy_element_to_clipboard


    // {{{ fn determine_yaml_file_path
    fn determine_yaml_file_path(
        &self,
    ) -> String {
        match shellexpand::full(
            &self.config.yaml_file,
        ) {
            Ok(expanded_yaml_file_path) => expanded_yaml_file_path.to_string(),
            Err(err)                    => {
                eprintln!("Unable to fully expand yaml file path: {err}");
                process::exit(49);
            },
        }
    }
    // }}} fn determine_yaml_file_path


    // {{{ fn extract_yaml_content
    fn extract_yaml_content(
        &self,
        yaml_file_path: &String,
    ) -> String {
        let yaml_content = if ! self.config.preprocessor_command.is_empty() {
            self.preprocess_yaml_file(
                &self.config.preprocessor_command,
                yaml_file_path,
            )
        }
        else {
            match fs::read_to_string(
                yaml_file_path,
            ) {
                Ok(yaml_file_content) => yaml_file_content,
                Err(err)              => {
                    eprintln!("Unable to read file '{yaml_file_path}' to string: {err}");
                    process::exit(50);
                },
            }
        };

        if self.config.print_yaml_file {
            println!("{yaml_content}");
        }

        yaml_content
    }
    // }}} fn extract_yaml_content


    // {{{ fn preprocess_yaml_file
    fn preprocess_yaml_file(
        &self,
        preprocessor_command: &str,
        yaml_file_path:       &str,
    ) -> String {
        let mut arguments: VecDeque<&str> = preprocessor_command
            .split_whitespace()
            .collect()
        ;

        let preprocessed_yaml = if let Some(command) = arguments.pop_front() {
            arguments.push_back(yaml_file_path);

            let output = Command::new(command)
                .args(&arguments)
                .output()
            ;

            let output = match output {
                Ok(output) => output,
                Err(err)   => {
                    eprintln!("Unable to run command '{command}' with arguments '{arguments:?}': {err}");
                    process::exit(51);
                },
            };

            if output.status.success() {
                match String::from_utf8(output.stdout) {
                    Ok(preprocessed_yaml) => preprocessed_yaml,
                    Err(err)              => {
                        eprintln!("Unable to convert stdout to string: '{err}'");
                        process::exit(52);
                    },
                }
            }
            else {
                let stdout = match String::from_utf8(output.stdout) {
                    Ok(stdout) => stdout,
                    Err(err)   => {
                        eprintln!("Unable to convert stdout to string: '{err}'");
                        process::exit(53);
                    },
                };
                let stderr = match String::from_utf8(output.stderr) {
                    Ok(stderr) => stderr,
                    Err(err)   => {
                        eprintln!("Unable to convert stderr to string: '{err}'");
                        process::exit(54);
                    },
                };
                eprintln!(
                    "Preprocessor command '{}' with arguments '{:?}' exited with unexpected status: {}.",
                    command,
                    arguments,
                    output.status,
                );
                if ! stdout.is_empty() {
                    eprintln!(
                        "stdout: {}",
                        stdout.trim(),
                    );
                }
                if ! stderr.is_empty() {
                    eprintln!(
                        "stderr: {}",
                        stderr.trim(),
                    );
                }
                process::exit(55);
            }
        }
        else {
            eprintln!("Unable to get command from arguments '{arguments:?}'");
            process::exit(56);
        };
        preprocessed_yaml
    }
    // }}} fn preprocess_yaml_file
}
// }}} impl Extractor
